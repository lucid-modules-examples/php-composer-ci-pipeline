#!/bin/sh

# Enable pcov as cobertura coverage engine
php -dpcov.enabled=1 \
    -dpcov.directory=. \
    -dpcov.exclude="~vendor~" \
     ./vendor/bin/phpunit --coverage-text  --coverage-cobertura=coverage.cobertura.xml
