# About
WIP
This is an example GitLab CI pipeline for a PHP project.

## Features
- Running PHPUnit tests
- Running PHPStan

## Running locally
This will download the latest docker image from the docker registry and execute `bin/phpunit.sh`:
```shell
docker run --rm -v $(pwd):/app -w /app lucidmodules/composer-php-8.2 /app/bin/phpunit.sh
```

## How to adapt to your project
Required composer packages:

### PHPUnit
```shell
composer require --dev phpunit/phpunit phpunit/php-code-coverage
```

Note about Cobertura: the `@covers` annotation requires specifying FQN to the class or class with function.

### PHPStan
The default level for PHPStan is set to 9. Read more about [levels in the docs](https://phpstan.org/user-guide/rule-levels).
```shell
composer require --dev phpstan/phpstan
```
