<?php

declare(strict_types=1);

namespace LucidModules\Tests\PhpGitlabExample\Unit;

use LucidModules\PhpGitlabExample\Example;
use PHPUnit\Framework\TestCase;

class ExampleTest extends TestCase
{
    private Example $example;

    protected function setUp(): void
    {
        $this->example = new Example();
    }

    /**
     * @covers \LucidModules\PhpGitlabExample\Example::add
     * @return void
     */
    public function testAdd(): void
    {
        $this->assertEquals(5, $this->example->add(2, 3 ));
    }
}
