<?php

declare(strict_types=1);

namespace LucidModules\PhpGitlabExample;

class Example
{
    /**
     * @param int $a
     * @param int $b
     * @return int
     */
    public function add(int $a, int $b): int
    {
        return $a + $b;
    }
}
